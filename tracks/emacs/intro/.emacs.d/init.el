(require 'olivetti)
(require 'org-glossary)
(require 'org-modern)
(setq
 ;; User name and email
 user-full-name "User Name"
 user-mail-address "fixme@broken"

 ;; User interface
 visible-bell 1
 inhibit-splash-screen t

 ;; Make warnings about native compilation silent
 native-comp-async-report-warnings-errors 'silent

 ;; Customize title format of frames (also inserting SWWS_LESSON env variable)
 frame-title-format (concat "[swws-edu]" (getenv "SWWS_LESSON") ": %b")
 icon-title-format (concat "[swws-edu]" (getenv "SWWS_LESSON") ": %b")
 
 ;; Org styling, hide markup etc.
 org-hide-emphasis-markers t
 org-pretty-entities t
 org-ellipsis "…")

;; Set the default face height to 12pt
(set-face-attribute 'default nil :height 120)

;; Use visual line mode globally
(global-visual-line-mode)

;; Open lesson index file
(find-file-other-frame "/home/student/lesson/index.org")
