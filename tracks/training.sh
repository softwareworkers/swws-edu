#!/bin/sh
ROOT_DIR=${PWD}
CHANNELS=${ROOT_DIR}"/../channels.scm"
export SWWS_LESSON=${1}

XAUTH_HOME="$HOME/.Xauthority"
if [ -f $XAUTH_HOME ]; then
    OPTION_XAUTH="--expose="${XAUTH_HOME}
else
    OPTION_XAUTH="--expose="${XAUTHORITY}
fi

guix time-machine --no-offload -C ${CHANNELS} -- \
     shell --container --network --link-profile --no-cwd \
     --user=student \
     --preserve='^SWWS_' \
     --preserve='^DISPLAY$' \
     --preserve='^XAUTHORITY$' \
     $OPTION_XAUTH \
     --expose=${ROOT_DIR}/${SWWS_LESSON}=$HOME/lesson \
     --expose=$HOME=$HOME/"Home on host (RO)" \
     --share=${ROOT_DIR}"/../student-workspace"=$HOME/Persistent \
     --share=${ROOT_DIR}/${SWWS_LESSON}/.emacs.d=$HOME/.emacs.d \
     -m ${SWWS_LESSON}/manifest.scm -- \
     emacs
