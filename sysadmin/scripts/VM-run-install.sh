#!/bin/sh
#
# Cross platform script (GNU/Linux and MSYS2) to run a virtual
# machine (VM) if it's raw img file is present or install it if not.

# PREREQUISITES: This script use QEMU with accel enabled: please check
# if it's available and enabled on your machine BIOS.
#
# On MS Windows:
# 1. enable Hyper-V acceleration (https://learn.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)
# 2. install MYSYS2 (https://www.msys2.org/)
# 3. install QEMU (https://www.qemu.org/download/#windows)
#
# On GNU/Linux:
# 1. install QEMU using your distro package manager
# 2. accel support is enabled by default

# Parameters:
# - $1: name of the VM
# - $2: ISO file of the DVD/CDROM (installation media)

VM_NAME=$1
VM_CD_ISO=$2  # i.e.: guix-system-install-ac61e97.x86_64-linux.iso

# VM allocated resources
# TODO: allow to configure VM resources via config file or CLI flags.
VM_MEM="6G"
VM_CORES="2"
VM_SIZE="100G"

SHARE_DIR="../../student-workspace"
VM_DIR=${SHARE_DIR}"/machines"

# Get host OS
OS=$(uname -o)

# Set some variables depending on host OS
#
# VIRTFS01 option is to share (RW) SHARE_DIR with guest VM
# see https://wiki.qemu.org/Documentation/9psetup for details
# NOTE: does not work on Windows, see https://gitlab.com/qemu-project/qemu/-/issues/974
# FIXME: on Windows use SMB built-in server, see https://www.qemu.org/docs/master/system/invocation.html#hxtool-5
case $OS in
    GNU/Linux)
	ACCEL="kvm"
	IMG_FORMAT="raw" # raw image is much more efficient on COW host filesystems (BTRFS)
	EXTENSION="img"
	CPU="host"
	VIRTFS01="-virtfs local,path="${SHARE_DIR}",mount_tag=host0,security_model=mapped,id=host0,writeout=immediate,fmode=660,dmode=770,multidevs=forbid"
	SMB01=""
	;;
    Msys)
	ACCEL="whpx,kernel-irqchip=off"
	IMG_FORMAT="raw" # Windows NTFS do support holes, only allocated sectors get disk space
	EXTENSION="img"
	CPU="qemu64"
	VIRTFS01=""
	SMB01=",smb="${SHARE_DIR} # FIXME!
	;;
    *)
	ACCELL="null"
	IMG_FORMAT="null"
	EXTENSION="null"
	CPU="null"
	;;
esac

# Create VM image if don't exists or exit.
VM_IMAGE=${VM_DIR}/${VM_NAME}"."${EXTENSION}
if [ ! -e "$VM_IMAGE" ]
then
    while true; do
	echo "\n$VM_IMAGE not found."
	read -p "Do you wish to create an empty one? (Y/N)" yn
	case $yn in
            [Yy] ) mkdir -p ${VM_DIR};
		   qemu-img create -f ${IMG_FORMAT} ${VM_IMAGE} ${VM_SIZE};
		   break;;
            [Nn] ) echo "\nBye\n";
		   exit;;
            * ) echo "\nPlease answer Y (yes) or N (no).";;
	esac
    done
fi

# Boot the virtual machine using the installation image ad CDROM and
# the image as the drive
echo "Running $VM_NAME guest on ${OS} host..."
qemu-system-x86_64 \
     -cpu ${CPU} \
     -m ${VM_MEM} \
     -smp ${VM_CORES} \
     -device intel-hda \
     -device hda-duplex \
     -nic user,hostfwd=tcp::2222-:22 \
     -boot menu=on \
     -drive file=${VM_IMAGE},format=${IMG_FORMAT} \
     -drive media=cdrom,file=${VM_CD_ISO} \
     ${VIRTFS01} \
     -accel ${ACCEL}
