# SWWS Courses.

A collection of free (as in freedom) courses and related documentation.

**WARNING**: this project is still in alpha, pleas forgive us for the rough edges!

## Prerequisites.

All the software needed to attend the courses is managed via **[Guix](https://guix.gnu.org/)**: if you are not already using Guix System[^1] and have root permissions on your GNU/Linux distribution, you can install it on top of your existing distribution[^2] following the [installation section](https://guix.gnu.org/en/manual/en/html_node/Installation.html) of the [Guix manual](https://guix.gnu.org/en/manual/en/html_node/index.html#SEC_Contents).

Last but not least, to start each course you must have **basic** knowledge on how to start a terminal emulator, navigate through directories and start software via the command line interface (CLI).

## Install and update courses.

First you have to clone this repository in your home directory:

``` shell
cd ~ 
git clone https://gitlab.com/softwareworkers/swws-edu.git
```

Then please remember to **regularly update** the courses content via `git pull`.

**WARNING**: you can obviously rename `swws-edu` or clone it to a directory of your choiche, please just remember that the directory where you cloned the repository is the new project root directory, and adjust your CLI commands accordingly.

## How to start lessons.

Courses are divided in **tracks**, represented by top level directories (e.g. `emacs`, sysadmin), and **lessons**, represented by second level directories in each track (e.g. `emacs/intro` is the lesson named "intro" in the `emacs` track).

To start a lesson, open a terminal and:

``` shell
cd ~/swws-edu/tracks/
./training.sh <track>/<lesson>
```

where `<track>` is one of the available tracks and `<lesson>` is one of the lessons available in the track.  In your terminal emulator you can press =TAB= to have a list of tracks and use your shell autocompletion on directories.

### Student home and workspace.

In your working directory[^3] there is a folder named `student-workspace`[^4]: you can use that folder to **exchange documents** and other files with the containers created for each lesson you started.  The `student-workspace` folder is "mounted" as `/home/student/Persistent` inside each container.

**Warning**: the `home` folder of the `student` user (that is `/home/student`) in each container is **ephemeral**, this means that new files and directories created there - except the ones created in `/home/student/Persistent` - will be **deleted upon lesson exit**.

## How it works.

The `training.sh` script starts a container (via `guix shell`) with all the software needed for the lesson; the container environment is completely isolated from your working environment, this means you can only see and work on the documents provided with the courses: you cannot remove, edit or create documents outside of the container environment, so you can experiment without fears.

[^1]: installed as a full [operating system](https://guix.gnu.org/en/manual/en/html_node/System-Installation.html) on your machine or virtual machine.

[^2]: called "foreign distro" in Guix terms.

[^3]: the one where you cloned the git repository, i.e. swws-edu.

[^4]: that folder is ignored by git commands, i.e. `git status` will not list new or changed files in that folder.
